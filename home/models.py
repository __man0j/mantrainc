from __future__ import absolute_import, unicode_literals

from django.db import models
from django.utils.translation import ugettext as _

from modelcluster.fields import ParentalKey
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

IMAGE_CHOISES = (
    ("LEFT", _("left")),
    ("RIGHT", _("right"))
)

class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        InlinePanel('carousel_images', label="Carousel images"),
        FieldPanel('body', classname="full"),
    ]


class CarouselImage(Orderable):
        page = ParentalKey(HomePage, related_name='carousel_images')
        image = models.ForeignKey(
            'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
        )
        caption = models.CharField(blank=True, max_length=250)

        panels = [
            ImageChooserPanel('image'),
            FieldPanel('caption'),
        ]

class ServicePage(Page):
    content_panels = Page.content_panels + [

        InlinePanel('services', label="Add service")
    ]


class ProductsPage(Page):
    content_panels = Page.content_panels + [

        InlinePanel('products', label="Add product")
    ]

class Product(Orderable):
    page = ParentalKey(ProductsPage, related_name='products')
    name = models.CharField(blank=True, max_length=250)
    image_side = models.CharField(choices = IMAGE_CHOISES, blank=True, max_length=25, null=True)
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+', blank=True, null=True
    )
    description = RichTextField(blank=True)

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('image'),
        FieldPanel('image_side'),
        FieldPanel('description', classname="full")
    ]

class Service(Orderable):
    page = ParentalKey(ServicePage, related_name='services')
    name = models.CharField(blank=True, max_length=250)
    image_side = models.CharField(choices = IMAGE_CHOISES, blank=True, max_length=25, null=True)
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+', blank=True, null=True
    )
    description = RichTextField(blank=True)

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('image'),
        FieldPanel('image_side'),
        FieldPanel('description', classname="full")
    ]
